#pragma once
#ifndef _KOLEJKA_INKREMENTUJACA_  
#define _KOLEJKA_INKREMENTUJACA_    

#include <iostream>
#include <string>
using namespace std;

template <typename E>
class kolejka_tab_incrementujaca
{
private:
	E*tab;
	int size;
public:
	kolejka_tab_incrementujaca()
	{
		size = 0;
		tab = NULL;
	}

	~kolejka_tab_incrementujaca()
	{
		del_all();
	}

	void set_tab(E*Tab)
	{
		tab = Tab;
	}

	E* get_tab()
	{
		return tab;
	}

	int get_size()
	{
		return size;
	}

	bool empty()
	{
		if (tab == NULL)
			return true;
		else
			return false;
	}

	void inque(E ele)
	{
		E*tmp;
		tmp = get_tab();
		set_tab(new E[get_size() + 1]);
		for (int i = 0;i <get_size(); i++)
			get_tab()[i] = tmp[i];
		get_tab()[get_size()] = ele;
		delete[] tmp;
		size++;
	}

	E outque()
	{
		if (get_size() == 0)
		{
			string wyjatek = "nie ma elementow do zdjecia!";
			throw wyjatek;
		}
		else if (get_size() == 1)
		{
			E ele = get_tab()[0];
			delete[] get_tab();
			set_tab(NULL);
			size--;
			return ele;
		}
		else
		{
			E*tmp;
			tmp = get_tab();
			set_tab(new E[get_size() - 1]);
			for (int i = 1;i < get_size(); i++)
				get_tab()[i-1] = tmp[i];
			E ele = tmp[0];
			delete[] tmp;
			size--;
			return ele;
		}
	}

	void print()
	{
		for (int i = 0;i <get_size(); i++)
			cout << get_tab()[i] << endl;
	}

	void del_all()
	{
		while (get_size() != 0)
			outque();
	}
};




#endif 