#pragma once
#pragma once
#ifndef _KOLEJKA_LISTA_  
#define _KOLEJKA_LISTA_    


#include <iostream>
#include <string>

using namespace std;

template<typename E>
class wezel
{
private:
	int element;
	wezel<E>* next;
public:
	wezel()
	{
		element = 0;
		next = NULL;
	}

	E get_element()
	{
		return element;
	}

	void set_element(E elem)
	{
		element = elem;
	}

	wezel<E>* get_next()
	{
		return next;
	}

	void set_next(wezel<E>* nastepny)
	{
		next = nastepny;
	}
};

template<typename E>
class koliejka
{
private:
	wezel<E>* head;
	wezel<E>* tail;
	int s;
public:
	koliejka()
	{
		tail = NULL;
		head = NULL;
		s = 0;
	}

	~koliejka()
	{
		this->delete_all();
	}

	bool empty()
	{
		if (head == NULL)
			return true;
		else
			return false;
	}

	void add_back(const E& dana)
	{
		if (tail == NULL)
		{
			head = tail = new wezel <E>;
			head->set_element(dana);
			head->set_next(NULL);
		}
		else
		{
			tail->set_next(new wezel <E>);
			tail->get_next()->set_element(dana);
			tail->get_next()->set_next(NULL);
			tail = tail->get_next();
		}
		s++;
	}

	E front()
	{
		E dana;
		if (head == NULL)
		{
			string wyjatek = "E front() :pusta  lista !";
			throw wyjatek;
		}
		else
		{
			wezel<E>* tmp = head;
			E dana = head->get_element();
			head = head->get_next();
			delete tmp;
			s--;
			return dana;
		}
	}

	int size()
	{
		return s;
	}

	void print()
	{
		wezel<E>* tmp = head;
		while (tmp != NULL)
		{
			cout << tmp->get_element() << endl;
			tmp = tmp->get_next();
		}
	}

	void delete_all()
	{
		while (head != NULL)
			this->front();
	}

};


#endif 