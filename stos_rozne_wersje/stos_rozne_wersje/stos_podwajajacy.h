#pragma once
#ifndef _STOS_PODWAJAJACY_ 
#define _STOS_PODWAJAJACY_    

#include <iostream>
#include <string>
using namespace std;

template <typename E>
class stos_tab_podwojona
{
private:
	E*tab;
	int size;
	int size_max;
public:
	stos_tab_podwojona()
	{
		size = 0;
		tab = NULL;
		size_max = 1;
	}

	~stos_tab_podwojona()
	{
		del_all();
	}

	void set_tab(E*Tab)
	{
		tab = Tab;
	}

	E* get_tab()
	{
		return tab;
	}

	int get_size()
	{
		return size;
	}

	int get_size_max()
	{
		return size_max;
	}

	bool empty()
	{
		if (tab == NULL)
			return true;
		else
			return false;
	}

	void push(E ele)
	{
		if (get_size() == get_size_max() || get_size() == 0)
		{
			E*tmp = get_tab();
			set_tab(new E[get_size_max() * 2]);
			size_max = 2 * get_size_max();
			for (int i = 0;i < get_size(); i++)
				get_tab()[i] = tmp[i];
			get_tab()[get_size()] = ele;
			delete[] tmp;
		}
		else
			get_tab()[get_size()] = ele;
		size++;
	}

	E pop()
	{
		if (get_size() == 0)
		{
			string wyjatek = "nie ma elementow do zdjecia!";
			throw wyjatek;
		}
		if (get_size() == 1)
		{
			E ele = get_tab()[0];
			delete[] get_tab();
			set_tab(NULL);
			size--;
			return ele;
		}
		else
		{
			E*tmp = get_tab();
			set_tab(new E[get_size() - 1]);
			for (int i = 0;i < get_size() - 1; i++)
				get_tab()[i] = tmp[i];
			E ele = tmp[get_size() - 1];
			delete[] tmp;
			size--;
			size_max = get_size();
			return ele;
		}
	}

	void print()
	{
		for (int i = get_size() - 1;i >-1; i--)
			cout << get_tab()[i] << endl;
	}

	void del_all()
	{
		while (get_size() != 0)
			pop();
	}
};

#endif 