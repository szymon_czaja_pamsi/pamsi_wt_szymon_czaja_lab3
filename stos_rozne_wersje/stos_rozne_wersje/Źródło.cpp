#include <stack>
#include <time.h>
#include <windows.h>
#include "stos_inkrementujacy.h"
#include "stos_podwajajacy.h"
#include "kolejka_inkrementujaca.h"
#include "kolejka_podwajajaca.h"
#include "kolejka_lista.h"



int main()
{
	int ile = 500000;
	srand(time(NULL));
	clock_t start, stop;
	
	try 
	{
		stos_tab_incrementujaca<int> stos;
		stos_tab_podwojona<int> stos2;
		stack <int> stos3;

		cout << "ilosc operacji: " << ile << endl<<endl;

		start = clock();
		for (int i = 0;i < ile; i++)
			stos.push(rand() % 100+1);
		stop = clock();

		cout << "stos inkrementujacy: " << (double)(stop - start) / CLOCKS_PER_SEC <<  "s" <<endl; 

		start = clock();
		for (int i = 0;i < ile; i++)
			stos2.push(rand() % 100+1);
		stop = clock();

		cout << "stos podwajajacy: " << (double)(stop - start) / CLOCKS_PER_SEC << "s" << endl;

		start = clock();
		for (int i = 0;i < ile; i++)
			stos3.push(rand() % 100+1);
		stop = clock();
		
		cout << "stos STL: " << (double)(stop - start) / CLOCKS_PER_SEC << "s" << endl << endl;

//====================================KOLEJKA=========================================

		kolejka_tab_incrementujaca<int> kolej;
		kolejka_tab_podwojona<int> kolej2;
		koliejka <int> kolej3;



		start = clock();
		for (int i = 0;i < ile; i++)
			kolej.inque(rand() % 100 + 1);
		stop = clock();

		cout << "kolejka inkrementujaca: " << (double)(stop - start) / CLOCKS_PER_SEC << "s" << endl;

		start = clock();
		for (int i = 0;i < ile; i++)
			kolej2.inque(rand() % 100 + 1);
		stop = clock();

		cout << "kolejka podwajajaca: " << (double)(stop - start) / CLOCKS_PER_SEC << "s" << endl;

		start = clock();
		for (int i = 0;i < ile; i++)
			kolej3.add_back(rand() % 100 + 1);
		stop = clock();

		cout << "kolejka oparta na liscie: " << (double)(stop - start) / CLOCKS_PER_SEC << "s" << endl;


	}
	catch (string w)
	{
		cout << "Wyjatek: " << w << endl;
	}

	system("PAUSE");
	return 0;
}


