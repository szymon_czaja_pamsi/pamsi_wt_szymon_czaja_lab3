
#include "stos.h"

void wyswietl(stos <int>* s1, stos <int>* s2, stos <int>* s3)
{
	cout<<endl << "wyswietlam wszystkie stosy:" <<endl;
	cout << "PIERWSZA" << endl;
	s1->print();
	cout <<"DRUGA"  << endl;
	s2->print();
	cout << "TRZECIA" << endl;
	s3->print();
}

void wieza(int ele, stos <int>* s1, stos <int>* s2, stos <int>* s3)
{
//	wyswietl(s1, s2, s3);
	if (ele > 0)
	{
		wieza(ele - 1, s1, s3, s2);
		s3->push(s1->pop());
		wyswietl(s1, s2, s3);
		wieza(ele - 1, s2, s1, s3);
	}
	//wyswietl(s1, s2, s3);
}


int main()
{
	int ile = 3;
	stos <int> s1,s2,s3;
	for (int i = 0;i < ile;i++)
		s1.push(ile-i);


	wyswietl(&s1, &s2, &s3);
	wieza(s1.get_size(), &s1, &s2, &s3);
	wyswietl(&s1, &s2, &s3);
	
	system("PAUSE");
	return 0;
}