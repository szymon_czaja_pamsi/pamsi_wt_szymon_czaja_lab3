
#ifndef _STOS_
#define _STOS_

#include <iostream>
#include <string>

using namespace std;

template <typename E>
	class stos
	{
	private:
		E*tab;
		int size;
	public:
		stos();

		~stos();
		
		void set_tab(E*Tab);

		E* get_tab();

		int get_size();

		bool empty();

		void push(E ele);

		E pop();

		void print();

		void del_all();

		E operator [](int i);
};




	template <typename E>
	stos<E>::stos()
	{
		size = 0;
		tab = NULL;
	}

	template <typename E>
	stos<E>::~stos()
	{
		del_all();
	}

	template <typename E>
	void stos<E>::set_tab(E*Tab)
	{
		tab = Tab;
	}

	template <typename E>
	E* stos<E>::get_tab()
	{
		return tab;
	}

	template <typename E>
	int stos<E>::get_size()
	{
		return size;
	}

	template <typename E>
	bool stos<E>::empty()
	{
		if (tab == NULL)
			return true;
		else
			return false;
	}

	template <typename E>
	void stos<E>::push(E ele)
	{
		E*tmp;
		tmp = get_tab();
		set_tab(new E[get_size() + 1]);
		for (int i = 0;i < get_size(); i++)
			get_tab()[i] = tmp[i];
		get_tab()[get_size()] = ele;		delete[] tmp;
		size++;
	}

	template <typename E>
	E stos<E>::pop()
	{
		if (get_size() == 0)
		{
			string wyjatek = "nie ma elementow do zdjecia!";
			throw wyjatek;
		}
		else if (get_size() == 1)
		{
			E ele = get_tab()[0];
			delete[] get_tab();
			set_tab(NULL);
			size--;
			return ele;
		}
		else
		{
			E*tmp;
			tmp = get_tab();
			set_tab(new E[get_size() - 1]);
			for (int i = 0;i < get_size() - 1; i++)
				get_tab()[i] = tmp[i];
			E ele = tmp[get_size() - 1];
			delete[] tmp;
			size--;
			return ele;
		}
	}

	template <typename E>
	void stos<E>::print()
	{
		for (int i = get_size() - 1;i > -1; i--)
			cout << get_tab()[i] << endl;
	}

	template <typename E>
	void stos<E>::del_all()
	{
		while (get_size() != 0)
			pop();
	}

	template <typename E>
	E stos<E>::operator [](int i)
	{
		E ele;
		ele = get_tab()[i];
		return ele;
	}

#endif

